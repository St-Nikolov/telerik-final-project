package API.weare;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import API.weare.models.*;


public class WEAreAPI {
    private final String WEARE_API = "http://localhost:8081/api";

    public String registerUser(UserDTOModel user){
        String response = RestAssured.given().contentType(ContentType.JSON)
                .body(user).log().everything()
                .when().post(WEARE_API + "/users/")
                .then().log().everything()
                .statusCode(200)
                .extract().response().asString();
        return response;
    }

    public UserModel getUserByIDandName(int id, String userName){
        UserModel response = RestAssured
                .when().get(WEARE_API + "/users/auth/{id}" + "?principal={principal}",id,userName)
                .then().log().everything()
                .statusCode(200)
                .extract().response().as(UserModel.class);
        return response;
    }

    public String authenticate(UserModel user){
         String cookie = RestAssured.given()
                 .formParam("username","UserOne")
                 .formParam("password","123456")
                 .when().post("http://localhost:8081/authenticate")
                 .then().statusCode(302)
                 .extract().response().getCookie("JSESSIONID");
         return cookie;
    }

    public void logOut(){
        RestAssured.when().post("http://localhost:8081/logout")
                .then().statusCode(302);
    }
//    public String deleteUser(){
//
//    }

//    public void getSkills(){
//        RestAssured
//                .when().get(WEARE_API + "/skill/")
//                .then().log().everything()
//                .statusCode(200)
//                .extract().response().asString();
////        return response;
//
//    }

//    public PostModel createPost(PostDTOModel post, String name){
//        PostModel response = RestAssured.given()
//                .auth().basic("UserEight", "123456")
//                .contentType(ContentType.JSON)
//                .body(post).log().everything()
//                .when().post(WEARE_API + "/post/auth/creator" )
//                .then().log().everything()
//                .statusCode(200)
//                .extract().response().as(PostModel.class);
//        return response;
//    }
//
//    public PostModel[] getUserPosts(PageModel page, int userId){
//        PostModel[] response = RestAssured
//                .given().contentType(ContentType.JSON)
//                .body(page).log().everything()
//                .when().get(WEARE_API + "/users/{id}/posts", userId)
//                .then().log().everything()
//                .statusCode(200)
//                .extract().response().as(PostModel[].class);
//        return response;
//    }
//
//    public ExpertiseProfileModel updateUserExpertiseProfile(ExpertiseProfileModel model,int userId){
//        ExpertiseProfileModel response = RestAssured
//                .given()
//                .auth().basic("UserEight","123456")
//                .contentType(ContentType.JSON)
//                .body(model).log().everything()
//                .when().post(WEARE_API + "/users/auth/{id}/expertise", userId)
//                .then().log().everything()
//                .statusCode(200)
//                .extract().response().as(ExpertiseProfileModel.class);
//        return response;
//
//    }
}