package API.weare.models;

import java.util.ArrayList;

public class UserModel {
    public int  id;
    public String username;
    public ArrayList< String > authorities = new ArrayList();
    public String email;
    public String firstName = null;
    public String lastNAme = null;
    public String gender = null;
    public String city = null;
    public String birthYear = null;
    public String personalReview = null;
    public String expertise;
    public ArrayList < String > skills = new ArrayList ();

}
