package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class HomePage extends BasePage {

    public HomePage(UserActions actions) {
        super(actions, "weare.homePage");
    }

    public void assertRegisterButtonVisible(){
        actions.waitForElementVisible("weare.homePage.navBar.register");
    }

    public void clickSignInButton(){
        actions.clickElement("weare.homePage.navBar.signIn");
    }

    public void clickLogOutButton(){
        actions.clickElement("weare.homePage.navBar.logOut");
    }

    public void openProfilePage(){
        actions.clickElement("weare.homePage.navBar.profileButton");
    }

    public void openAdminZone() {
        actions.waitForElementVisible("weare.homePage.goToAdminZone");
        actions.clickElement("weare.homePage.goToAdminZone");
    }

    public void openViewUsersPage(){
        actions.assertElementPresent("weare.homePage.adminViewUsersButton");
        actions.clickElement("weare.homePage.adminViewUsersButton");
    }

    public void clickNewPostButton(){
        actions.clickElement("weare.homePage.navBar.addPostButton");
    }

    public void clickLatestPostsButton(){
        actions.clickElement("weare.homePage.navBar.latestPostsButton");
    }

    public void clickRegisterButton(){
        actions.clickElement("weare.homePage.registerButton");
    }
    public void searchForExistingUserByName(String name){
        actions.typeValueInField(name, "weare.homePage.searchNameField");
        actions.clickElement("weare.homePage.searchButton");
    }

    public void searchForExistingUserByProfession(String profession){
        actions.typeValueInField(profession, "weare.homePage.searchProfessionField");
        actions.clickElement("weare.homePage.searchButton");
    }

    public void searchForAllExistingUsers(){
        actions.clickElement("weare.homePage.searchButton");
    }

    public void assertForExistingUserByName(String name){
        actions.assertElementPresent("weare.searchResults.name", name);
    }

    public void assertForExistingUserByProfession(String profession){
        actions.assertElementPresent("weare.searchResults.profession", profession);
    }

    public void assertUserNotFound(){
        actions.assertElementPresent("weare.searchResults.noUserExistingMessage");
    }

    public void assertUserIsLoggedIn(){
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.homePage.navBar.profileButton");
    }



}
