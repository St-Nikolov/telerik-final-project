package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class ExplorePostPage extends BasePage {
    public ExplorePostPage(UserActions actions) {
        super(actions, "weare.explorePostPage");
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.explorePostPage.editPostButton");
    }

    public void assertPostImageLoaded(){
        actions.assertImageIsLoaded("weare.explorePostPage.postImage" );
    }
    public void clickEditPostButton(){
        actions.clickElement("weare.explorePostPage.editPostButton");
    }

    public void clickDeleteButton(){
        actions.clickElement("weare.explorePostPage.deletePostButton");
    }

    public void leaveComment(String content){
        actions.typeValueInField(content,"weare.explorePostPage.commentTextArea");
        actions.clickElement("weare.explorePostPage.postCommentButton");

    }

    private int parseNumberOfComemntsLabel(String msg){
        String[] words = msg.split("\\s");
        return Integer.parseInt(words[0]);
    }


}
