package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class ViewUsersPage extends BasePage {

    public ViewUsersPage(UserActions actions) {
        super(actions, "weare.viewUsersPage");
    }

    public void assertForExistingUserByName(String name){
        actions.assertElementPresent("weare.searchResults.name", name);
    }
}
