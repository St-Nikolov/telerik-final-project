package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class CreatePostPage extends BasePage {

    public CreatePostPage(UserActions actions) {
        super(actions, "weare.createPostPage");
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.createNewPostPage.savePostButton");
    }

    public void createPost(String isPrivate, String content,String imgPath){
        actions.selectValueFromDropdown(isPrivate.trim().toLowerCase().equals("private")?  "Private post" : "Public post",
                "weare.createNewPostPage.postVisibilityDropdown");
        actions.typeValueInField(content==null ? "" : content,"weare.createNewPostPage.contentTextField");
        if (imgPath!=null && !imgPath.trim().isEmpty())
            actions.uploadPhoto("weare.createNewPostPage.imageFileInput",imgPath);
        actions.clickElement("weare.createNewPostPage.savePostButton");
    }

    public void assertPostIsNotCreated(){
       assertPageNavigated();
       actions.waitForElementVisible("weare.createNewPostPage.ErrorMessage");
    }

}
