package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class ProfilePage extends BasePage {

    public ProfilePage(UserActions actions) {
        super(actions, "weare.profilePage");
    }


    public void clickEditProfileButton(){
        actions.waitForElementVisible("weare.profilePage.editProfileButton");
        actions.clickElement("weare.profilePage.editProfileButton");
    }

    public void clickSeeProfileButton(){
        actions.waitForElementVisible("weare.profilePage.seeProfileButton");
        actions.clickElement("weare.profilePage.seeProfileButton");
    }

    public void clickConnectButton(){
        actions.waitForElementVisible("weare.profilePage.connectButton");
        actions.clickElement("weare.profilePage.connectButton");
    }

    public void clickDisconnectButton(){
        actions.waitForElementVisible("weare.profilePage.disconnectButton");
        actions.clickElement("weare.profilePage.disconnectButton");
    }

    public void clickDisableButton(){
        actions.waitForElementVisible("weare.profilePage.adminDisableButton");
        actions.clickElement("weare.profilePage.adminDisableButton");
    }

    public void clickEnableButton() {
        actions.clickElement("weare.profilePage.adminEnableButton");
    }

    public void clickNewFriendsRequestButton(){
        actions.clickElement("weare.profilePage.newFriendRequestButton");
    }

    public void assertFields(String name, String birthday, String text, String city){
        actions.assertElementPresent("weare.profilePage.assertName", name);
        actions.assertElementPresent("weare.profilePage.assertBirthday", birthday);
        actions.assertElementPresent("weare.profilePage.assertText", text);
        actions.assertElementPresent("weare.profilePage.assertCity", city);
    }

    public void assertCategory(String professionalCategory){
        actions.assertElementPresent("weare.profilePage.assertCategory",professionalCategory);
    }

    public void assertServiceAndHours(String service, String hours){
        actions.assertElementPresent("weare.profilePage.assertService", service);
        actions.assertElementPresent("weare.profilePage.assertTime", hours);
    }

    public void assertForOneAddedFriendInTheList(){
        actions.assertElementPresent("weare.profilePage.assertForOneFriendInTheFriendsList");
    }

    public void assertConnectButtonIsVisible(){
        actions.assertElementPresent("weare.profilePage.connectButton");
    }

    public void assertProfilePictureIsNotBlank() {
        actions.assertElementPresent("weare.profilePage.assertUserImageIsNotBlank");
    }

}
