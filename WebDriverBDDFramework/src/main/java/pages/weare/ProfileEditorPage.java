package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class ProfileEditorPage extends BasePage {

    public ProfileEditorPage(UserActions actions) {
        super(actions, "weare.profileEditorPage");
    }

    public void enterFirstName(String name){
        actions.deleteValueInField("weare.profileEditorPage.firstnameInput");
        actions.typeValueInField(name, "weare.profileEditorPage.firstnameInput");
    }

    public void enterLastName(String name){
        actions.deleteValueInField("weare.profileEditorPage.lastnameInput");
        actions.typeValueInField(name, "weare.profileEditorPage.lastnameInput");
    }

    public void enterBirthday(String birthday){
        actions.typeValueInField(birthday, "weare.profileEditorPage.birthdayInput");
    }

    public void selectGender(String gender){
        actions.selectValueFromDropdown(gender, "weare.profileEditorPage.genderInput");
    }

    public void enterFewWordsAboutYourself(String text){
        actions.deleteValueInField("weare.profileEditorPage.textInput");
        actions.typeValueInField(text, "weare.profileEditorPage.textInput");
    }

    public void selectCity(String city){
        actions.selectValueFromDropdown(city, "weare.profileEditorPage.cityInput");
    }

    public void clickUpdateMyProfileButton(){
        actions.clickElement("weare.profileEditorPage.submitButton");
    }

    public void enterProfessionalCategory(String professionalCategory){
        actions.selectValueFromDropdown(professionalCategory, "weare.profileEditorPage.categoryField");
    }

    public void clickUpdateCategoryButton(){
        actions.clickElement("weare.profileEditorPage.updateCategoryButton");
    }

    public void enterService(String services){
        actions.deleteValueInField("weare.profileEditorPage.serviceField");
        actions.typeValueInField(services, "weare.profileEditorPage.serviceField");
    }

    public void enterWeeklyAvailability(String hours){
        actions.clickElement("weare.profileEditorPage.weeklyAvailability");
        actions.deleteValueInField("weare.profileEditorPage.weeklyAvailability");
        actions.typeValueInField(hours, "weare.profileEditorPage.weeklyAvailability");
    }

    public void clickUpdateAvailabilityButton(){
        actions.clickElement("weare.profileEditorPage.updateAvailabilityButton");
    }

    public void uploadPhoto(){
        actions.uploadPhoto("weare.profileEditorPage.uploadPhoto","src/test/resources/images/profilePic.png");
    }

    public void clickUpdatePhotoButton(){
        actions.clickElement("weare.profileEditorPage.uploadPhotoButton");
    }

    public void clickProfileButton() {
        actions.clickElement("weare.profileEditorPage.profileButton");
    }
}
