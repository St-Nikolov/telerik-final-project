package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class LoginPage extends BasePage {

    public LoginPage(UserActions actions) {
        super(actions, "weare.loginPage");
    }

    public void enterUsername(String username) {
        actions.typeValueInField(username, "weare.loginPage.usernameInput");
    }

    public void enterPassword(String password) {
        actions.typeValueInField(password, "weare.loginPage.passwordInput");
    }

    public void clickLoginButton(){
        actions.clickElement("weare.loginPage.loginButton");
    }

    public void assertWrongUsernameOrPasswordMessage() {
        actions.assertElementPresent("weare.loginPage.wrongUsernameOrPasswordMessage");
    }

    public void assertSuccessfulLogOut(){
        actions.waitForElementVisible("weare.loginPage.logoutConfirmMessage");
    }
}
