package pages.weare;

import com.telerikacademy.testframework.UserActions;
import pages.BasePage;

public class RegistrationFormPage extends BasePage {

    public RegistrationFormPage(UserActions actions) {
        super(actions, "weare.registerPage");
    }

    @Override
    public void assertPageNavigated() {
        super.assertPageNavigated();
        actions.waitForElementVisible("weare.registerPage.header");
    }

    public void assertSuccessfulRegistration(){
        actions.waitForElementVisible("weare.registerPage.success");
    }

    public void fillRegistrationForm(String userName, String email, String password, String confirmPassword, String category){
        actions.typeValueInField(userName==null ? "" : userName,"weare.registerPage.userNameInput");
        actions.typeValueInField(email==null ? "" : email,"weare.registerPage.emailInput");
        actions.typeValueInField(password==null ? "" : password,"weare.registerPage.passwordInput");
        actions.typeValueInField(confirmPassword==null ? "" : confirmPassword,"weare.registerPage.confirmPasswordInput");
        if (category!=null && !category.isEmpty()) {
            actions.selectValueFromDropdown(category,"weare.registerPage.categoryDropDown");
        }
        actions.clickElement("weare.registerPage.registerButton");
    }


    public String getErrorMessage(){
        return actions.getWebElement("weare.registerPage.errorMessage").getText();
    }

    public String generateValidUniqueUserName(){
        StringBuffer userName = new StringBuffer("TestUser");
        long timestamp = System.currentTimeMillis();
        while (timestamp!=0) {
            userName.append((char)(timestamp%26+'A'));
            timestamp/=26;
        }
        return userName.toString();
    }
}
