package com.telerikacademy.testframework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.WebDriverManager;


public class CustomWebDriverManager {
    public enum CustomWebDriverManagerEnum {
        INSTANCE;
        private WebDriver driver = setupBrowser();

        private WebDriver setupBrowser() {
            String browser = System.getProperty("browser");
            if (browser==null) {
                WebDriverManager.chromedriver().setup();
                WebDriver chromeDriver = new ChromeDriver();
                chromeDriver.manage().window().maximize();
                driver = chromeDriver;
                return new Driver(chromeDriver);
            }
            switch (browser) {
                case "firefox": {
                    WebDriverManager.firefoxdriver().setup();
                    WebDriver firefoxDriver = new FirefoxDriver();
                    firefoxDriver.manage().window().maximize();
                    driver = firefoxDriver;
                    return new Driver(firefoxDriver);
                }
                case "chrome": {
                    WebDriverManager.chromedriver().setup();
                    WebDriver chromeDriver = new ChromeDriver();
                    chromeDriver.manage().window().maximize();
                    driver = chromeDriver;
                    return new Driver(chromeDriver);
                }
                default: {
                    Utils.LOG.error("Browser not recognized, check config.properties file");
                    return null;
                }
            }
        }

        public void quitDriver() {
            if (driver != null) {
                driver.quit();
                driver = null;
            }
        }

        public WebDriver getDriver() {
            if (driver == null) {
                setupBrowser();
            }
            return driver;
        }


    }
}
