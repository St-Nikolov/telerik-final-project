package com.telerikacademy.testframework;


import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;
    private Actions actions;
    private WebDriverWait explicitWait;

    public UserActions() {
        System.out.println("Create UserActions");
        this.driver = Utils.getWebDriver();
        actions = new Actions(driver);
        explicitWait = new WebDriverWait(driver, Long.parseLong(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds")));

    }

    public static void loadBrowser(String baseUrlKey) {
        System.out.println("UserActions - Load browser");
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key, Object... arguments) {

        WebElement element = getWebElement(key, arguments);
        Utils.LOG.info("Clicking on element " + key);
        element.click();
    }

    public void hoverElement(String locatorKey, Object... arguments) {

        WebElement element = getWebElement(locatorKey,arguments);
        Utils.LOG.info("Hover an element " + locatorKey);
        actions.moveToElement(element);
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        WebElement element = getWebElement(field,fieldArguments);
        Utils.LOG.info("Typing " + value + " in: " + field);
        element.clear();
        element.sendKeys(value);
    }

    public void deleteValueInField(String field, Object... fieldArguments){
        WebElement element = getWebElement(field,fieldArguments);
        element.sendKeys(Keys.CONTROL,"a");
        element.sendKeys(Keys.DELETE);
    }

    public void selectValueFromDropdown(String value, String field, Object... fieldArguments){
        Select dropdown = new Select(getWebElement(field,fieldArguments));
        dropdown.selectByVisibleText(value);
        Utils.LOG.info("Select " + value + " in: " + field + " drop down menu.");
    }

    public void switchToIFrame(String iframe) {

        WebElement iFrame = getWebElement(iframe);
        Utils.LOG.info("Switching to iFrame: " + iframe);
        driver.switchTo().frame(iFrame);
    }

    public void switchToDefaultFrame() {
        Utils.LOG.info("Switching back to default frame");
        driver.switchTo().defaultContent();
    }


        //############# WAITS #########

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        waitForElementVisibleUntilTimeout(getLocatorValueByKey(locatorKey),
                Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds")),
                arguments);
    }

    public void waitForElementVisibleUntilTimeout(String locatorKey, int seconds, Object... locatorArguments) {
        By xpathLocator = By.xpath(locatorKey);
        explicitWait.withTimeout(Duration.ofSeconds(seconds)).
                until(ExpectedConditions.presenceOfElementLocated(xpathLocator));
        try {
            explicitWait.withTimeout(Duration.ofSeconds(seconds)).
                    until(ExpectedConditions.visibilityOfElementLocated(xpathLocator));
        } catch (TimeoutException ex) {
            Utils.LOG.error("Web element with xpath : " + locatorKey + " is not visible.");
            throw ex;
        }
    }

    public void waitForElementPresent(String locatorKey, Object... arguments) {
        By xpathLocator = By.xpath(locatorKey);
        try {
            explicitWait.until(ExpectedConditions.presenceOfElementLocated(xpathLocator));
        } catch (TimeoutException ex) {
            Utils.LOG.error("Web element with xpath : " + locatorKey + " is not present in the DOM.");
            throw ex;
        }

    }

    public boolean isElementPresent(String locatorKey, Object... arguments) {
        By xpathLocator = By.xpath(locatorKey);
        try {
            explicitWait.until(ExpectedConditions.presenceOfElementLocated(xpathLocator));
            return true;
        } catch (TimeoutException ex) {
            return false;
        }


    }

    public boolean isElementVisible(String locatorKey, Object... arguments) {
        By xpathLocator = By.xpath(locatorKey);
        try {
            explicitWait.until(ExpectedConditions.visibilityOfElementLocated(xpathLocator));
            return true;
        } catch (TimeoutException ex) {
            return false;
        }

    }

    public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locatorKey, Object... arguments) {
        Assertions.assertNotNull(getWebElement(locatorKey, arguments));
    }

    public void assertElementAttribute(String locatorKey, String attributeName, String attributeValue) {
        WebElement element = getWebElement(locatorKey);
        String valueOfAttribute = element.getAttribute(attributeName);
        Assertions.assertEquals(valueOfAttribute, attributeValue,"Attribute value is not equal!");
    }

    public void assertNavigatedUrl(String urlKey) {
        String currentUrl = driver.getCurrentUrl();
        Assertions.assertTrue(driver.getCurrentUrl().equals(urlKey),"Navigated URL is different!");
    }

    public void pressKey(Keys key) {
        Actions actions = new Actions(driver);
        actions.keyDown(key).perform();
        actions.keyUp(key).perform();
    }

    public void assertImageIsLoaded(String locatorKey, Object... arguments){
        waitForElementVisible(locatorKey,arguments);
        WebElement image = getWebElement(locatorKey,arguments);
        String source = image.getAttribute("src");
        boolean isImage = false;
        try {
            BufferedImage img = ImageIO.read(new URL(source));
            isImage = true;
        } catch(IllegalArgumentException e){

        } catch (IOException e) {
            Utils.LOG.error("Image at URL: " + source + "couldn't be load /n" + e.getMessage());
        }
        Assertions.assertTrue(isImage);
    }

    public void assertMessage(String expected,String actual){
        Assertions.assertTrue(expected.equals(actual),String.format("'%s' message is expected, but '%s' is shown",expected,actual));
    }

    private String getLocatorValueByKey(String locator, Object... arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }

    public WebElement getWebElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        waitForElementVisible(locator);
        WebElement element = driver.findElement(By.xpath(locator));
        return element;
    }

    public void uploadPhoto(String locatorKey,String imagePath) {
        File profilePic = new File(imagePath);
        String absolutePath = profilePic.getAbsolutePath();
        WebElement element = getWebElement(locatorKey);
        element.sendKeys(absolutePath);
    }
}
