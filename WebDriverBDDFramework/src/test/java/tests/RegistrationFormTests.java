package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

public class RegistrationFormTests extends BaseTest {

    @BeforeAll
    public void navigateToPage(){
        home.clickRegisterButton();
        registrationFormPage.assertPageNavigated();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/test_data/register_invalid_data.csv", numLinesToSkip = 2)
    public void should_ShowProperMessage_when_RegisterWithInvalidData(String expectedMessage,String userName,String email,String password, String confirmPassword,String category) {
         registrationFormPage.fillRegistrationForm(userName, email, password, confirmPassword, category);

        if (expectedMessage!=null && !expectedMessage.isBlank()) {
            actions.assertMessage(expectedMessage,registrationFormPage.getErrorMessage());
            Assertions.assertTrue(expectedMessage.equals(registrationFormPage.getErrorMessage()),"");
        }
    }


    @ParameterizedTest
    @CsvSource({"abv@abv.bg,123456,Dentist"})
    public void registerUserWithRandomValidUsername(String email,String password,String category){
        registrationFormPage.fillRegistrationForm(registrationFormPage.generateValidUniqueUserName(),email,password,password,category);

        registrationFormPage.assertSuccessfulRegistration();
    }
}
