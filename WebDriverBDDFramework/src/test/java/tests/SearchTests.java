package tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SearchTests extends BaseTest {

    @BeforeEach
    public void signIn(){
        super.singIn();
    }

    @AfterEach
    public void logOut(){
        super.logOut();
    }

    @Test
    public void searchExistingUserByName() {
        home.searchForExistingUserByName("John Doe");

        home.assertForExistingUserByName("John Doe");
    }

    @Test
    public void searchExistingUserByProfession() {
        home.searchForExistingUserByProfession("Accountant");

        home.assertForExistingUserByProfession("Accountant");
    }

    @Test
    public void searchForAllExistingUsers() {
        home.searchForAllExistingUsers();

        home.assertForExistingUserByName("John Doe");
    }

    @Test
    public void searchForNonExistingProfileByName() {
        home.searchForExistingUserByName("Ivan Georgiew");

        home.assertUserNotFound();
    }

}
