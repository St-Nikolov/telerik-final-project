package tests;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EditProfileTests extends BaseTest {

    @BeforeEach
    public void signIn(){
        super.singIn();
    }

    @AfterEach
    public void logOut(){
        super.logOut();
    }

    @Test
    public void editPersonalProfile_when_UserIsLogged(){
        home.openProfilePage();
        profile.clickEditProfileButton();
        edit.enterFirstName("John");
        edit.enterLastName("Doe");
        edit.enterBirthday("01011990");
        edit.selectGender("MALE");
        edit.enterFewWordsAboutYourself("Sample text");
        edit.selectCity("Plovdiv");
        edit.clickUpdateMyProfileButton();
        edit.clickProfileButton();

        profile.assertFields("John Doe", "1990-01-01", "Sample text", "Plovdiv");

    }

    @Test
    public void editProfessionalCategory_when_UserIsLogged(){
        home.openProfilePage();
        profile.clickEditProfileButton();
        edit.enterProfessionalCategory("Accountant");
        edit.clickUpdateCategoryButton();

        profile.assertCategory("Accountant");
    }

    @Test
    public void updateUserServices_when_UserIsLogged(){
        home.openProfilePage();
        profile.clickEditProfileButton();
        edit.enterService("QA");
        edit.enterWeeklyAvailability("20.0");
        edit.clickUpdateAvailabilityButton();

        home.navigateToPage();
        home.openProfilePage();
        profile.assertServiceAndHours("QA","20.0");
    }

    @Test
    public void uploadProfilePicture_when_UserIsLogged(){
        home.openProfilePage();
        profile.clickEditProfileButton();
        edit.uploadPhoto();
        edit.clickUpdatePhotoButton();

        profile.assertProfilePictureIsNotBlank();
    }

}

