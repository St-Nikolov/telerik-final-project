package tests;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

public class PostsTests extends BaseTest {

    private String latestValidPostContent;

    @Order(1)
    @ParameterizedTest
    @CsvFileSource(resources = "/test_data/create_posts_data.csv", delimiter = ',',numLinesToSkip = 1)
    public void createPost_when_ContentAndPictureAreProvided(String isValid,String isPrivate,String content,String imgPath) {
        singIn();

        home.clickNewPostButton();
        createPostPage.assertPageNavigated();
        createPostPage.createPost(isPrivate,content,imgPath);

        if (isValid.trim().toLowerCase().equals("valid")) {
            postsPage.assertPageNavigated();
            latestValidPostContent = content;
        }
        else createPostPage.assertPostIsNotCreated();
        if (imgPath!=null && !imgPath.isEmpty()) {
            postsPage.explorePostByContent(content);
            explorePostPage.assertPostImageLoaded();
        }
    }

    @Order(2)
    @ParameterizedTest
    @CsvSource({"valid,private,Edited content,"})
    public void editPost_when_ContentAndPictureAreProvided(String isValid,String isPrivate,String content,String imgPath) {

        home.navigateToPage();
        home.clickLatestPostsButton();
        postsPage.assertPageNavigated();
        postsPage.explorePostByContent(latestValidPostContent);
        explorePostPage.assertPageNavigated();

        explorePostPage.clickEditPostButton();
        editPostPage.assertPageNavigated();
        editPostPage.editPost(isPrivate,content,imgPath);

        if (isValid.trim().toLowerCase().equals("valid")) {
            explorePostPage.assertPageNavigated();
            latestValidPostContent = content;
        }
        else editPostPage.assertPostIsNotEdited();
        if (imgPath!=null && !imgPath.isEmpty()) {
            postsPage.explorePostByContent(content);
            explorePostPage.assertPostImageLoaded();
        }

    }

    @Test
    @Order(3)
    public void deletePost() {
        home.navigateToPage();
        home.clickLatestPostsButton();
        postsPage.assertPageNavigated();
        postsPage.explorePostByContent(latestValidPostContent);
        explorePostPage.assertPageNavigated();

        explorePostPage.clickDeleteButton();
        deletePostConfirmationPage.assertPageNavigated();
        deletePostConfirmationPage.confirmDelete(true);

        deletePostConfirmationPage.assertPostIsDeleted();
        logOut();
    }
}

