package stepDefinitions;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class AdminStepDefinitions extends BaseStepDefinition {



    @When("Login with $userName account with password $password")
    public void autoLogin(String userName, String password){
        home.navigateToPage();
        home.clickSignInButton();
        login.enterUsername(userName);
        login.enterPassword(password);
        login.clickLoginButton();
    }

    @When("Open admin zone")
    public void openAdminZone(){
        home.openAdminZone();
    }

    @When("Open 'View Users' page")
    public void openViewUsersPage(){
        home.openViewUsersPage();
    }

    @Then("List of all registered users is shown")
    public void assertExistingUser(){
        users.assertForExistingUserByName("John Doe");
        home.clickLogOutButton();
    }

    @When("Search for existing user")
    public void searchForExistingUser(){
        home.searchForExistingUserByName("Jane Doe");
    }

    @When("Open user profile and disable it")
    public void openUsersProfile(){
        profile.clickSeeProfileButton();
        profile.clickDisableButton();
    }

    @Then("Logout from admin account and try to login with the disabled account")
    public void tryToLoginWithTheDisabledAccount(){
        home.clickLogOutButton();
        login.enterUsername("Jane Doe");
        login.enterPassword("123456");
        login.clickLoginButton();
        login.assertWrongUsernameOrPasswordMessage();
        login.enterUsername("admin");
        login.enterPassword("123456");
        login.clickLoginButton();
        searchForExistingUser();
        profile.clickSeeProfileButton();
        profile.clickEnableButton();
        home.clickLogOutButton();
        login.assertSuccessfulLogOut();
    }
}
