package stepDefinitions;

import API.weare.WEAreAPI;
import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.*;
import pages.weare.HomePage;
import pages.weare.LoginPage;
import pages.weare.ProfilePage;
import pages.weare.ViewUsersPage;

public class BaseStepDefinition {


    UserActions actions = new UserActions();
    HomePage home = new HomePage(actions);
    LoginPage login = new LoginPage(actions);
    ProfilePage profile = new ProfilePage(actions);
    ViewUsersPage users = new ViewUsersPage(actions);
    WEAreAPI weAreAPI = new WEAreAPI();



    @AfterStories
    public static void tearDown(){
        UserActions.quitDriver();
    }


    @Given("WEare homepage is loaded")
    public void homePageIsVisible(){
        home.navigateToPage();
        home.assertRegisterButtonVisible();
    }

}
