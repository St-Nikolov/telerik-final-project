package stepDefinitions;

import org.jbehave.core.annotations.*;

public class LogInStepDefinitions extends BaseStepDefinition{



    @When("Click on Sign In button")
    public void openSignInPage(){
        home.clickSignInButton();
        login.assertPageNavigated();
    }

    @When("Fill in $userName in User Name field and $password in Password field and click 'Login'")
    public void fillInCredentials(String userName,String password){
        login.enterUsername(userName);
        login.enterPassword(password);
        login.clickLoginButton();
    }

    @Then("Home page with logged in user is loaded")
    public void assertSuccessfulLogIn(){
        home.assertPageNavigated();
        home.assertUserIsLoggedIn();
    }

    @Then("User is not logged in and proper message is displayed")
    public void assertUnSuccessfulLogIn(){
        login.assertPageNavigated();
        login.assertWrongUsernameOrPasswordMessage();
    }

}
