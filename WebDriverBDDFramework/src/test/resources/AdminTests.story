Meta:
@AdminTests

Scenario: Open 'View Users' page as an admin of the social network.

Given WEare homepage is loaded
When Login with admin account with password 123456
When Open admin zone
When Open 'View Users' page
Then List of all registered users is shown

Scenario: Disable user

Given WEare homepage is loaded
When Login with admin account with password 123456
And Search for existing user
And Open user profile and disable it
Then Logout from admin account and try to login with the disabled account