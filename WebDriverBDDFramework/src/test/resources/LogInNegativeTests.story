Meta:
@LogInTests
@Negative

Narrative:
As a user
I shouldn't be able to log in with wrong credentials

Scenario: Check that user cannot log in with wrong or empty credentials and propper message is displayed

Given WEare homepage is loaded
When Click on Sign In button
When Fill in <UserName> in User Name field and <Password> in Password field and click 'Login'
Then User is not logged in and proper message is displayed

Examples:
|UserName|Password|
|||
||123456|
|UserOne||
|UserOne|12345|
|User|123456|
