Meta:
@LogInTests

Narrative:
As a registered user in WEare Social Network
I should be able to log in with my credentials

Scenario: Check if registered user can successfuly log in using proper credentials

Given WEare homepage is loaded
When Click on Sign In button
When Fill in UserOne in User Name field and 123456 in Password field and click 'Login'
Then Home page with logged in user is loaded


